class Album < ActiveRecord::Base
  include Rails.application.routes.url_helpers
  include NotificationsHelper
  # define constants
  DEF_ALBUMS_PER_PAGE = 6

# Validations
  validates :title, :description, presence: true
  validates :publish, inclusion: { in: [true,false] }
# Associations
  belongs_to :user
  has_many :images, dependent: :destroy
  has_many :likes, as: :likeable
  has_many :follows, as: :followable

  paginates_per DEF_ALBUMS_PER_PAGE

  scope :default_album, ->{ where(title: "default") }
  scope :published, ->{ where(publish: true) }
  default_scope { where.not(title: "default") }

  after_create :get_create_album_nofitication, if: :published?
  after_destroy :get_destroy_album_nofitication, if: :published?

  def default?
    self.title == 'default'
  end
end
