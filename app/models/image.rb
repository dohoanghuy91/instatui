class Image < ActiveRecord::Base
  # define constants
  DEF_IMGS_PER_PAGE = 8
  PAGINATING_WINDOW = 2
  has_attached_file :img_content, styles: { thumb: "200x200>" }, default_url: "/images/404.png"
  validates_attachment_content_type :img_content, content_type: /\Aimage\/.*\z/
  validates_attachment_presence :img_content

# Validations
  validates :publish, inclusion: { in: [true, false] }
# Associations
  belongs_to :album
  has_many :likes, as: :likeable

  scope :published, ->{ where(publish: true) }
  paginates_per DEF_IMGS_PER_PAGE
end
