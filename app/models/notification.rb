class Notification < ActiveRecord::Base
  # define constants
  NOTIFICATIONS_PER_PAGE = 8;
  belongs_to :user
  scope :unread, ->{ where(readed: false) }
  default_scope { order(created_at: :desc) }

  paginates_per NOTIFICATIONS_PER_PAGE
end
