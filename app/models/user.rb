class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  include NotificationsHelper

  # define constants
  DEF_USERS_PER_PAGE = 8
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
# Validations
  validates :name, presence: true
# Associations
  has_many :albums, dependent: :destroy
  has_many :images, through: :albums
  has_many :follows, as: :followable
  has_many :notifications
#
  default_scope { order(:id) }
# Callbacks
  after_create :create_default_album
  paginates_per DEF_USERS_PER_PAGE

  def create_default_album
    self.albums.create(title: "default", description: "default description");
  end

  def admin?
    self.permission == 'admin'
  end

  def active_for_authentication?
    super && active?
  end

  def add_album_notification(type, album ,fromuser, link)
    content = "#{fromuser.name} #{type} #{album.title}"

    self.notifications.create(notification_type: type, content: content, link: link)
  end
end
