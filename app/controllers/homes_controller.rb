class HomesController < ApplicationController
  # define constants
  IMGS_PER_PAGE = 12
  before_action :authenticate_user!,
    only: [
            :new, :create, :image_create, :like_image, :like_album,
            :follow_album, :follow_user, :clear_notifications
          ]
  before_action :find_album, only: [:like_album, :follow_album]
  before_action :find_image, only: [:like_image]
  before_action :find_user, only: [:clear_notifications, :follow_user, :show_publish_user]


  def index
    @publish_images = Image.published.page(params[:page])
  end

  def new
    @albums = current_user.albums
  end

  # Show an publish image
  def show
    not_found, @image, @owner, @album, @liked = ImageService.publish_image_info(params[:id], current_user)
    redirect_to :back if not_found
  end

  # Show an publish album
  def show_publish_album
    not_found, @album, @owner, @images, @liked, @followed = AlbumService.publish_album_info(params[:id], current_user,params[:album_img_page])
    redirect_to :back if not_found
  end

  # Show an publish user
  def show_publish_user
    @images, @followed = UserService.publish_user_info(@user, current_user, params[:album_img_page])
  end

  def like_image
    flash[:notice] = UserService.like_an_image(@image, current_user)
    respond_to do |format|
      format.js
    end
  end

  def like_album
    flash[:notice] = UserService.like_an_image(@album, current_user)
    respond_to do |format|
      format.js
    end
  end

  def follow_user
    flash[:notice] = UserService.follow_a_user(@user, current_user)
    respond_to do |format|
      format.js
    end
  end

  def clear_notifications
    flash[:notice] = UserService.clear_itself_notifications(current_user)

    respond_to do |format|
      format.js
    end
  end

  def create
    upload_service = UploadService.new(params, current_user)
    flash[:notice] = upload_service.upload_new_album
    redirect_to new_homes_path
  end

  def image_create
    upload_service = UploadService.new(params, current_user)
    flash[:notice] = upload_service.upload_new_image
    redirect_to new_homes_path
  end
end
