class ImagesController < ApplicationController
  # define constants
  before_action :authenticate_user!, only: [:index, :show, :update, :destroy]
  before_action :find_image, only: [:show, :update, :destroy]

  def index
    @images = current_user.images.page(params[:imagepage])
    @albums = current_user.albums.page(params[:albumpage]).includes(:images)
  end

  def show
    render file: 'public/404.html', status: :not_found, layout: false unless @image
    @publish = @image.publish
  end

  def update
    flash.now[:notice] = ImageService.update_existing_image(@image, image_params)
    redirect_to :back
  end

  def destroy
    if @image.destroy
      flash.now[:notice] = I18n.t('successfully_delete')
      redirect_to homes_images_path
    else
      flash.now[:notice] = I18n.t('fail_delete')
      redirect_to :back
    end

  end

  private
    def image_params
      params.require(:updated_image).permit(:title, :description, :publish);
    end
end
