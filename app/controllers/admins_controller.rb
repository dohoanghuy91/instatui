class AdminsController < ApplicationController

  before_action :authenticate_user!, :check_permission, only: [:index, :update, :destroy]
  before_action :find_user, only: [:update, :destroy]
  before_action :create_user_service, only: [:update, :destroy]

  def index
    @images = Image.page(params[:imagepage])
  end

  def update
    flash.now[:notice] = @user_service.change_active_status
    redirect_to :back
  end

  def destroy
    flash.now[:notice] = @user_service.delete_user
    redirect_to :back
  end

  def manage_users
    @users = User.page(params[:userpage])
  end

  private
    def check_permission
      redirect_to root_path unless current_user.admin?
    end

    def create_user_service
      @user_service = UserService.new(@user)
    end
end
