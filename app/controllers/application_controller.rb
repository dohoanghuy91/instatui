class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  # Define constants
  LATEST_PAGE = 1
  before_action :store_previous_page, :load_notifications, if: :user_signed_in?

  def store_previous_page
    user_session[:return_to] ||= request.referer
  end

  def load_notifications
    @notifications = current_user.notifications.page(LATEST_PAGE)
  end

  private
    def find_album
      @album = Album.find_by(id: params[:id])
      check_not_found(@album, root_path)
    end

    def find_image
      @image = Image.find_by(id: params[:id])
      check_not_found(@image, root_path)
    end

    def find_user
      @user = User.find_by(id: params[:id])
      check_not_found(@user, root_path)
    end

    def check_not_found(item, return_path)
      unless item
        return redirect_to return_path
      end
    end
end
