class AlbumsController < ApplicationController
  include NotificationsHelper
  # Define constants
  IMGS_PER_PAGE = 12;
  before_action :authenticate_user!, only: [:show, :update, :destroy]
  before_action :find_album, only: [:update, :destroy]
  before_action :create_album_service, only: [:destroy, :show]

  def show
    not_found, @album, @default, @images = @album_service.get_detail(params[:album_img_page])
    redirect_to root_path if not_found
  end

  def update
    upload_service = UploadService.new(params, current_user)
    flash[:notice] = upload_service.update_exiting_album(@album)
    redirect_to :back
  end

  def destroy
    success, flash[:notice] = @album_service.delete_album
    success ? (redirect_to homes_image_path) : (redirect_to :back)
  end

  private
    def album_params
      params.require(:album).permit(:title, :description, :publish)
    end

    def create_album_service
      album = Album.find_by(id: params[:id])
      @album_service = AlbumService.new(album)
    end
end
