class UploadService

  #before_action :set_up_image_params, only: [:upload_new_image]

  include Rails.application.routes.url_helpers
  def initialize(params, current_user)
    @params = params
    @current_user = current_user
  end

  def upload_new_album
    set_up_album_params
    if @upload_images
      Image.transaction do
        new_album = @current_user.albums.create(album_params);
        @upload_images.each do |img|
          new_album.images.create(title: "", description: "", publish: @publish, img_content: img)
        end
      end
      I18n.t('successfully_upload')
    else
      new_album = @current_user.albums.create(album_params);
      I18n.t('successfully_upload_with_out_image')
    end
  rescue => e
    I18n.t('fail_upload')
  end

  def update_exiting_album(existing_album)
    set_up_album_params
    old_size = existing_album.images.size
    Album.transaction do
      # update title/description/publish of album
      existing_album.update(album_params)
      # update publish of all images in album
      images = existing_album.images
      images.each do |img|
        img.update(publish: @publish)
      end
      # add more images if uploaded
      if @upload_images
        @upload_images.each do |img|
          existing_album.images.create(title: "", description: "", publish: @publish, img_content: img)
        end
      end

      get_update_album_nofitication(existing_album, old_size, @current_user)
    end
    I18n.t('successfully_update')
  rescue => e
    I18n.t('fail_update')
  end

  def upload_new_image
    set_up_image_params
    if @album
      @album.images.create(image_params)
      I18n.t('successfully_upload')
    else
      @default_album.images.create(image_params)
      I18n.t('successfully_upload_to_def_album')
    end
  rescue => e
    I18n.t('fail_upload')
  end

  private
    def album_params
      @params.require(:album).permit(:title, :description, :publish)
    end

    def set_up_album_params
      @upload_images = @params[:album][:img_contents]
      @title = @params[:album][:title]
      @description = @params[:album][:description]
      @publish = @params[:album][:publish]
    end

    def image_params
      @params.require(:image).permit(:title, :description, :publish, :img_content)
    end

    def set_up_image_params
      @album = @current_user.albums.find_by(id: @params[:image][:album_id])
      @default_album = @current_user.albums.unscoped.default_album.first
    end

    def get_followers_of_user(user)
      user_ids = Follow.where(followable: user).pluck(:user_id)
      User.where(id: user_ids)
    end

    def get_update_album_nofitication(updated_album, old_size, current_user)
      followers = get_followers_of_user(current_user)
      new_size = updated_album.images.size
      no_of_new_imgs = new_size - old_size
      type = ''
      # no_of_new_imgs is a number, using to get number of photos after changing
      if no_of_new_imgs > 0
        type = I18n.t('adding_notification', photos_num: no_of_new_imgs)
      elsif no_of_new_imgs < 0
        type = I18n.t('removing_notification', photos_num: no_of_new_imgs)
      else
        type = I18n.t('updating_notification')
      end
      link = show_publish_album_homes_path(updated_album)
      followers.each do |follower|
        follower.add_album_notification(type ,updated_album, updated_album.user, link)
      end
    end
end
