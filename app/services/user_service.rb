class UserService

  def initialize(user)
    @user = user
  end

  # Change user active status
  def change_active_status
    @user.update(active: !@user.active)
    I18n.t('successfully_update')
  rescue => e
    I18n.t('fail_update')
  end

  # Delete user from database
  def delete_user
    @user.destroy
    I18n.t('successfully_delete')
  rescue => e
    I18n.t('fail_delete')
  end

  def self.publish_user_info(user, current_user, page_numer)
    images = user.images.published.page(page_numer)
    followed = Follow.find_by(followable: user, user: current_user).present?
    [images, followed]
  rescue => e
    false
  end

  def self.like_an_image (image, user_like)
    begin
      status = ''
      this_like = Like.find_by(likeable: image, user: user_like)
      unless this_like
        Like.create(likeable: image, user: user_like, like: true)
        status = I18n.t('liked')
      else
        this_like.destroy
        status = I18n.t('unliked')
      end
      status
    rescue
      false
    end
  end

  def self.like_an_album (album, user_like)
    begin
      status = ''
      this_like = Like.find_by(likeable: album, user: user_like)
      unless this_like
        Like.create(likeable: album, user: user_like, like: true)
        status = I18n.t('liked')
      else
        this_like.destroy
        status = I18n.t('unliked')
      end
      return status
    rescue
      false
    end
  end

  def self.follow_a_user (followed_user, user_follow)
    begin
      status = ''
      this_follow = Follow.find_by(followable: followed_user, user: user_follow)
      unless this_follow
        Follow.create(followable: followed_user, user: user_follow, follow: true)
        status = I18n.t('followed')
      else
        this_follow.destroy
        status = I18n.t('unfollowed')
      end
      return status
    rescue
      false
    end
  end

  def self.clear_itself_notifications (user)
    begin
      status = ''
      if user.notifications.update_all(readed: true)
        status = I18n.t('cleared')
      else
        status = I18n.t('cant_clear')
      end
      return status
    rescue
      false
    end
  end

end
