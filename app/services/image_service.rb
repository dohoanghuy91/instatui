class ImageService
  def initialize(params, current_user)
    @params = params
    @album = current_user.albums.find_by(id: params[:image][:album_id])
    @default_album = current_user.albums.unscoped.default_album.first
  end

  def self.publish_image_info (image_id, current_user)
    image = Image.published.find_by(id: image_id)
    unless image
      owner = album = liked = nil
    else
      owner = image.album.user
      album = image.album
      liked = Like.find_by(likeable: image, user: current_user).present?
    end

    [image.nil?, image, owner, album, liked]
  rescue => e
    false
  end

  def self.update_existing_image(image, image_params)
    image.update(image_params)
    I18n.t('successfully_update')
  rescue => e
    I18n.t('fail_update')
  end
end
