class AlbumService
  include Rails.application.routes.url_helpers
  IMGS_PER_PAGE = 12;
  def initialize(album)
    @album = album
  end

  def self.publish_album_info (album_id, current_user, page_number)
    album = Album.published.find_by(id: album_id)
    unless album
      owner = images = liked = followed = nil
    else
      owner = album.user
      images = album.images.page(page_number)
      liked = Like.find_by(likeable: album, user: current_user).present?
      followed = Follow.find_by(followable: album, user: current_user).present?
    end

    [album.nil?, album, owner, images, liked, followed]
  rescue => e
    false
  end

  def get_detail page_number
    #album = @album.includes(:images)
    album = Album.includes(:images).find_by(id: @album.id)
    default = album.default?
    images = album.images.page(page_number).per(IMGS_PER_PAGE)
    [album.nil?, album, default, images]
  rescue => e
    false
  end

  def delete_album
    @album.destroy
    [true, I18n.t('successfully_delete')]
  rescue => e
    [false, I18n.t('fail_delete')]
  end
end
