module NotificationsHelper
  def get_followers_of_user(user)
    user_ids = Follow.where(followable: user).pluck(:user_id)
    User.where(id: user_ids)
  end

  def get_followers_of_album(album)
    user_ids = Follow.where(followable: album).pluck(:user_id)
    User.where(id: user_ids)
  end

  #using in Album model
  def get_create_album_nofitication
    followers = get_followers_of_user(self.user)
    link = show_publish_album_homes_path(self)
    followers.each do |follower|
      follower.add_album_notification('created',self, self.user, link)
    end
  end

  #using in Album model
  def get_destroy_album_nofitication
    followers = get_followers_of_user(self.user)
    link = "#"
    followers.each do |follower|
      follower.add_album_notification('destroyed',self, self.user, link)
    end
  end

  #using in Image model
  def get_create_image_nofitication
    image_followers = get_followers_of(self.album)
    link = show_publish_album_homes_path(self)
    followers.each do |follower|
      follower.add_image_notification('created',self, self.user, link)
    end
  end

  #using in Album and Image models
  def published?
    self.publish
  end
end
