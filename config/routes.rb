Rails.application.routes.draw do
  root 'homes#index'

  resources :admins, only: [:index, :update, :destroy]
  resource :admins do
    get 'manage_users' => 'admins#manage_users'
  end

  #get 'admins/manage_users' => 'admins#manage_users'

  resource :homes, only: [:index, :new, :create] do
    resources :images, only: [:index, :show, :update, :destroy]
    resources :albums, only: [:show, :update, :destroy ]
    member do
      get 'new' , as: 'upload'
      post 'image_upload'            => 'homes#image_create'
      get 'show/:id'                => 'homes#show', as: 'show_image'
      post 'like_image/:id'         => 'homes#like_image', as: 'like_image'
      get 'show_publish_album/:id'  => 'homes#show_publish_album', as: 'show_publish_album'
      post 'like_album/:id'         => 'homes#like_album', as: 'like_album'
      post 'follow_album/:id'       => 'homes#follow_album', as: 'follow_album'
      post 'follow_user/:id'        => 'homes#follow_user', as: 'follow_user'
      post 'clear_notifications/:id'=> 'homes#clear_notifications', as: 'clear_notifications'
      get 'show_publish_user/:id'   => 'homes#show_publish_user', as: 'show_publish_user'

    end
  end
  #post 'homes/image_upload' => 'homes#image_create'

  #get 'homes/show/:id' => 'homes#show', as: 'show_image'
  #post 'homes/like_image/:id' => 'homes#like_image', as: 'like_image'

  #get 'homes/show_publish_album/:id' => 'homes#show_publish_album', as: 'show_publish_album'
  #post 'homes/like_album/:id' => 'homes#like_album', as: 'like_album'
  #post 'homes/follow_album/:id' => 'homes#follow_album', as: 'follow_album'
  #post 'homes/follow_user/:id' => 'homes#follow_user', as: 'follow_user'
  #post 'homes/clear_notifications/:id' => 'homes#clear_notifications', as: 'clear_notifications'

  #get 'homes/show_publish_user/:id' => 'homes#show_publish_user', as: 'show_publish_user'

  #get 'homes/new' , as: 'upload'


  devise_for :users, controllers: { registrations: 'users/registrations' }

end
