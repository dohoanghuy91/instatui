require 'rails_helper'

RSpec.describe Album, type: :model do
  let(:album) { Album.new }
  context 'isn\'t created as' do
    it 'without title' do
      album.description = "description of testing album"
      expect(album.valid?).to be false
    end

    it 'without description' do
      album.title = "Testing album"
      expect(album.valid?).to be false
    end

    it 'invalid publish type' do
      album.title = "Testing album"
      album.description = "description of testing album"
      album.publish = nil
      expect(album.valid?).to be false
    end


  end

  context 'is created successful as' do
    it 'have title and description' do
      album.title = "Testing album"
      album.description = "description of testing album"
      expect(album.valid?).to be true
    end

    it 'allow publish type is number' do
      album.title = "Testing album"
      album.description = "description of testing album"
      album.publish = 12
      expect(album.valid?).to be true
    end

    it 'allow publish type is string' do
      album.title = "Testing album"
      album.description = "description of testing album"
      album.publish = 'string'
      expect(album.valid?).to be true
    end
  end
end
