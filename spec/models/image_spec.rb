require 'rails_helper'

RSpec.describe Image, type: :model do
  let(:image) { Image.new }
  let(:testing_image) { File.new(Rails.root + "spec/testing/testing_image.png")}
  let(:document_file) {}
  context 'isn\'t created as' do
    it 'without image content' do
      image.title = "Testing image title"
      image.description = "Testing image description"
      image.img_content = nil
      expect(image.valid?).to be false
    end

    it 'just accept image types' do
      image.title = "Testing image title"
      image.description = "Testing image description"
      image.img_content = document_file
      expect(image.valid?).to be false
    end

    it 'invalid publish type' do
      image.title = "Testing album"
      image.description = "description of testing album"
      image.img_content = testing_image
      image.publish = nil
      expect(image.valid?).to be false
    end

  end

  context 'is created successful as' do
    it 'allow don\'t have title and description' do
      image.img_content = testing_image
      expect(image.valid?).to be true
    end

    it 'all inputs valid' do
      image.title = "Testing image title"
      image.description = "Testing image description"
      image.publish = false
      image.img_content = testing_image
      expect(image.valid?).to be true
    end

    it 'allow publish type is number' do
      image.title = "Testing image title"
      image.description = "Testing image description"
      image.publish = 12
      image.img_content = testing_image
    end

    it 'allow publish type is string' do
      image.title = "Testing image title"
      image.description = "Testing image description"
      image.publish = "string"
      image.img_content = testing_image
    end

  end
end
