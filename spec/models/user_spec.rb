require 'rails_helper'

RSpec.describe User, type: :model do
  let(:user) { User.new }
  context 'isn\'t created as' do
    it 'without name' do
      user.email = "test@yopmail.com"
      user.password = "123456"
      expect(user.valid?).to be false
    end

    it 'without email' do
      user.name = "nguyen van a"
      user.password = "123456"
      expect(user.valid?).to be false
    end

    it 'without password' do
      user.email = "testuser@gmail.com"
      user.name = "nguyen van a"
      expect(user.valid?).to be false
    end

    it 'invalid email' do
      user.email = "testuser@gmail.com"
      user.password = "123456"
      expect(user.valid?).to be false
    end

    it 'too short password' do
      user.email = "testuser@gmail.com"
      user.name = "nguyen van a"
      user.password = "123"
      expect(user.valid?).to be false
    end
  end

  context 'is created successful as' do
    it 'all inputs is valid' do
      user.email = "test@yopmail.com"
      user.name = "nguyen van a"
      user.password = "123456"
      expect(user.valid?).to be true
    end
  end

end
