class CreateImages < ActiveRecord::Migration
  def up
    create_table :images do |t|
      t.belongs_to :album
      t.string :title
      t.string :description
      t.boolean :publish, default: true

      t.timestamps null: false
    end
    add_attachment :images, :img_content
    add_index :images, :album_id, name: 'album_id_ix'
  end

  def down
    remove_attachment :images, :img_content
    drop_table :images
  end

end
