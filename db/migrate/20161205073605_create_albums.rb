class CreateAlbums < ActiveRecord::Migration
  def up
    create_table :albums do |t|
      t.belongs_to :user
      t.string :title
      t.string :description
      t.integer :no_of_images, default: 0
      t.boolean :publish, default: true

      t.timestamps null: false
    end
    add_index :albums, :user_id, name: 'user_id_ix'
  end

  def down
      drop_table :albums
  end
end
