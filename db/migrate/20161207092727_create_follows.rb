class CreateFollows < ActiveRecord::Migration
  def up
   create_table :follows do |t|
     t.belongs_to :user
     t.boolean :follow
     t.references :followable, polymorphic: true
     t.timestamps
   end
   add_index :follows, [:followable_id, :followable_type]
   add_index :follows, :user_id
  end

  def down
    drop_table :follows
  end
end
