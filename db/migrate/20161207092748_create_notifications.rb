class CreateNotifications < ActiveRecord::Migration
  def up
    create_table :notifications do |t|
      t.belongs_to :user
      t.string :notification_type
      t.string :content
      t.string :link
      t.boolean :readed, default: false
      t.timestamps null: false
    end
    add_index :notifications, :user_id, name: 'noti_user_id_ix'
  end

  def down
    drop_table :notifications
  end
end
