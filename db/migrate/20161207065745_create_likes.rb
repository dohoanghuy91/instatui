class CreateLikes < ActiveRecord::Migration
  def up
   create_table :likes do |t|
     t.belongs_to :user
     t.boolean :like
     t.references :likeable, polymorphic: true
     t.timestamps
   end
   add_index :likes, [:likeable_id, :likeable_type]
   add_index :likes, :user_id
  end

  def down
    drop_table :likes
  end
end
